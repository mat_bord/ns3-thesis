import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
from statistics import mean
from IPython.display import display


sns.set_style("white")

parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='PlotComparison/'


#30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)
#Pckt received from drone / pckt I want to send - 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_30fps(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15*result['params']['numUe']
    #calculate total pckt to UEs
    tot_packet_received=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_30fps = campaign.get_results_as_dataframe(pckt_reliability_30fps, verbose=True )
display(prob_pckt_30fps)


#15fps

ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - 15 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_15fps(result):
    byteS   = result['params']['fileSize']*15; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15*result['params']['numUe']#15 is the simulation time
    #calculate total pckt to Ues
    tot_packet_received=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])      
    
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]


# Use the parsing function to create a Pandas dataframe
prob_pckt_15fps = campaign.get_results_as_dataframe(pckt_reliability_15fps,verbose=True )
display(prob_pckt_15fps)


#Broadcast30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_broadcast(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_broadcast = campaign.get_results_as_dataframe(pckt_reliability_broadcast,verbose=True)
display(prob_pckt_broadcast)


#Broadcast15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_broadcast15fps(result):
    byteS   = result['params']['fileSize']*15; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_broadcast15fps = campaign.get_results_as_dataframe(pckt_reliability_broadcast15fps,verbose=True)
display(prob_pckt_broadcast15fps)

#FrameToAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])

#In this scenario the number of pckts that arrives is not equal to the number of pckts to send: 
# to do a good calculus lets assign to TOT_PACKETS the total number of pckts that have to arrive at the end
def pckt_reliability_fta(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.033; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]
# Use the parsing function to create a Pandas dataframe
prob_pckt_frameToAnno = campaign.get_results_as_dataframe(pckt_reliability_fta,verbose=True)
display(prob_pckt_frameToAnno)

#Drop the useless column
prob_pckt_frameToAnno = prob_pckt_frameToAnno.drop('fileSizeAnno', 1)

#FrameToAnno15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])

#In this scenario the number of pckts that arrives is not equal to the number of pckts to send: 
# to do a good calculus lets assign to TOT_PACKETS the total number of pckts that have to arrive at the end
def pckt_reliability_fta15fps(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.066; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]
# Use the parsing function to create a Pandas dataframe
prob_pckt_frameToAnno15fps = campaign.get_results_as_dataframe(pckt_reliability_fta15fps,verbose=True)
display(prob_pckt_frameToAnno15fps)

#Drop the useless column
prob_pckt_frameToAnno15fps = prob_pckt_frameToAnno15fps.drop('fileSizeAnno', 1)


#OnlyAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)
#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_OnlyAnno(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.033; #how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to send to Ues
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
                
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    #print(tot_packet_received)
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    if (pckt_reliability_DB>100):
        pckt_reliability_DB=100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_OnlyAnno = campaign.get_results_as_dataframe(pckt_reliability_OnlyAnno,verbose=True)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
prob_pckt_OnlyAnno=prob_pckt_OnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in prob_pckt_OnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    prob_pckt_OnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(prob_pckt_OnlyAnno)


#OnlyAnno15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)
#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_OnlyAnno15fps(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.066; #how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to send to Ues
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
                
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    #print(tot_packet_received)
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    if (pckt_reliability_DB>100):
        pckt_reliability_DB=100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_OnlyAnno15fps = campaign.get_results_as_dataframe(pckt_reliability_OnlyAnno15fps,verbose=True)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
prob_pckt_OnlyAnno15fps=prob_pckt_OnlyAnno15fps.sort_values(by=['numUe'])
rngrun=0
for index, row in prob_pckt_OnlyAnno15fps.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    prob_pckt_OnlyAnno15fps.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(prob_pckt_OnlyAnno15fps)

#User X axes Plot
#Multiple latnecy into 1 plot
Reliability_allinone=pd.DataFrame()
prob_pckt_30fps['Type'] = 'Scenario1'
prob_pckt_15fps['Type'] = 'Scenario1_15fps'
prob_pckt_broadcast['Type'] = 'Scenario2'
prob_pckt_broadcast15fps['Type'] = 'Scenario2_15fps'
prob_pckt_frameToAnno['Type'] = 'Scenario3'
prob_pckt_frameToAnno15fps['Type'] = 'Scenario3_15fps'
prob_pckt_OnlyAnno['Type'] = 'Scenario4'
prob_pckt_OnlyAnno15fps['Type'] = 'Scenario4_15fps'
Reliability_allinone=Reliability_allinone.append(prob_pckt_30fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_broadcast)
Reliability_allinone=Reliability_allinone.append(prob_pckt_broadcast15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_frameToAnno15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_OnlyAnno)
Reliability_allinone=Reliability_allinone.append(prob_pckt_OnlyAnno15fps)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX.png',bbox_inches='tight')

#FileSize X axes Plot

#Multiple latnecy into 1 plot
Reliability_allinone=pd.DataFrame()
prob_pckt_30fps=prob_pckt_30fps.sort_values(by=['fileSize'])
prob_pckt_15fps=prob_pckt_15fps.sort_values(by=['fileSize'])
prob_pckt_broadcast=prob_pckt_broadcast.sort_values(by=['fileSize'])
prob_pckt_broadcast15fps=prob_pckt_broadcast15fps.sort_values(by=['fileSize'])
prob_pckt_frameToAnno=prob_pckt_frameToAnno.sort_values(by=['fileSize'])
prob_pckt_frameToAnno15fps=prob_pckt_frameToAnno15fps.sort_values(by=['fileSize'])
prob_pckt_OnlyAnno=prob_pckt_OnlyAnno.sort_values(by=['fileSize'])
prob_pckt_OnlyAnno15fps=prob_pckt_OnlyAnno15fps.sort_values(by=['fileSize'])

Reliability_allinone=Reliability_allinone.append(prob_pckt_30fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_broadcast)
Reliability_allinone=Reliability_allinone.append(prob_pckt_broadcast15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_frameToAnno15fps)
Reliability_allinone=Reliability_allinone.append(prob_pckt_OnlyAnno)
Reliability_allinone=Reliability_allinone.append(prob_pckt_OnlyAnno15fps)

g=sns.catplot(data=Reliability_allinone,
            x='fileSize',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=4)

g.set_xticklabels(rotation=80)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEnFileSizeX.png',bbox_inches='tight')


