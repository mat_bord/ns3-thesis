import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
from statistics import mean
from IPython.display import display


sns.set_style("white")

parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='PlotComparison/'


#perfect30fps
fileSize=[]
throughput=[]
numue=[]
perfect_throughput30fps=pd.DataFrame()
for i in range(0,len(parameters),2):
    fileSize.append(parameters[i+1])
    throughput.append(((parameters[i+1]+30)*30*8)/1000000) # I multiply frame size * fps * ...
    numue.append(parameters[i])
perfect_throughput30fps['fileSize']=fileSize
perfect_throughput30fps['Throughput [Mbit/s]']=throughput
perfect_throughput30fps['numUe']=numue

display(perfect_throughput30fps)

#30 FPS
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#2nd link throughput per user

#Instead of doing the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUser30fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

# Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUser30fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUser30fps,
                                            throughputPDCP_labelsUser)
display(PDCPlayer_throughputUser30fps)

#15 FPS
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUser15fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUser15fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUser15fps,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUser15fps)

#Total perfect throughput 1st link vs total throughput 1st link
#and 
#perfect throughput per user 1st link vs 1st link throughput per user
#valid for:
# broadcast frame link 1 and 2 as Perfect throughput per user
# broadcast frame link 1 as Total Perfect throughput

fileSize=[]
throughput=[]
perfect_throughput15fps=pd.DataFrame()
numue=[]
for i in range(0,len(parameters),2):
    fileSize.append(parameters[i+1])
    throughput.append(((parameters[i+1]+30)*15*8)/1000000) # I multiply frame size * fps * ...
    numue.append(parameters[i])

perfect_throughput15fps['fileSize']=fileSize
perfect_throughput15fps['Throughput [Mbit/s]']=throughput
perfect_throughput15fps['numUe']=numue
display(perfect_throughput15fps)

#Broadcast30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserBroadcast(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserBroadcast = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserBroadcast,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUserBroadcast)

#Broadcast15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserBroadcast15fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserBroadcast15fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserBroadcast15fps,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUserBroadcast15fps)


#FrameToAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserFtA(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserFrametoAnno = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserFtA,throughputPDCP_labelsUser)

#Drop the useless column
PDCPlayer_throughputUserFrametoAnno = PDCPlayer_throughputUserFrametoAnno.drop('fileSizeAnno', 1)


display(PDCPlayer_throughputUserFrametoAnno)

#FrameToAnno15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserFtA15fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserFrametoAnno15fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserFtA15fps,throughputPDCP_labelsUser)

#Drop the useless column
PDCPlayer_throughputUserFrametoAnno15fps = PDCPlayer_throughputUserFrametoAnno15fps.drop('fileSizeAnno', 1)


display(PDCPlayer_throughputUserFrametoAnno15fps)


#OnlyAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserOnlyAnno(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserOnlyAnno = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserOnlyAnno,throughputPDCP_labelsUser)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
PDCPlayer_throughputUserOnlyAnno=PDCPlayer_throughputUserOnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in PDCPlayer_throughputUserOnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    PDCPlayer_throughputUserOnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(PDCPlayer_throughputUserOnlyAnno)

#OnlyAnno15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserOnlyAnno15fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserOnlyAnno15fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserOnlyAnno15fps,throughputPDCP_labelsUser)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
PDCPlayer_throughputUserOnlyAnno15fps=PDCPlayer_throughputUserOnlyAnno15fps.sort_values(by=['numUe'])
rngrun=0
for index, row in PDCPlayer_throughputUserOnlyAnno15fps.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    PDCPlayer_throughputUserOnlyAnno15fps.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(PDCPlayer_throughputUserOnlyAnno15fps)



#User X axes Plot
UserBC_throughput=pd.DataFrame()

PDCPlayer_throughputUser30fps['Type'] = 'Scenario1'
PDCPlayer_throughputUser15fps['Type'] = 'Scenario1_15fps'
PDCPlayer_throughputUserBroadcast['Type'] = 'Scenario2'
PDCPlayer_throughputUserBroadcast15fps['Type'] = 'Scenario2_15fps'
PDCPlayer_throughputUserFrametoAnno['Type'] = 'Scenario3'
PDCPlayer_throughputUserFrametoAnno15fps['Type'] = 'Scenario3_15fps'
PDCPlayer_throughputUserOnlyAnno['Type'] = 'Scenario4'
PDCPlayer_throughputUserOnlyAnno15fps['Type'] = 'Scenario4_15fps'
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser30fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserBroadcast)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserBroadcast15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserFrametoAnno)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserFrametoAnno15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserOnlyAnno)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserOnlyAnno15fps)


sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX.png',bbox_inches='tight')

#FileSize X axes Plot
UserBC_throughput=pd.DataFrame()

PDCPlayer_throughputUser30fps=PDCPlayer_throughputUser30fps.sort_values(by=['fileSize'])
PDCPlayer_throughputUser15fps=PDCPlayer_throughputUser15fps.sort_values(by=['fileSize'])
PDCPlayer_throughputUserBroadcast=PDCPlayer_throughputUserBroadcast.sort_values(by=['fileSize'])
PDCPlayer_throughputUserBroadcast15fps=PDCPlayer_throughputUserBroadcast15fps.sort_values(by=['fileSize'])
PDCPlayer_throughputUserFrametoAnno=PDCPlayer_throughputUserFrametoAnno.sort_values(by=['fileSize'])
PDCPlayer_throughputUserFrametoAnno15fps=PDCPlayer_throughputUserFrametoAnno15fps.sort_values(by=['fileSize'])
PDCPlayer_throughputUserOnlyAnno=PDCPlayer_throughputUserOnlyAnno.sort_values(by=['fileSize'])
PDCPlayer_throughputUserOnlyAnno15fps=PDCPlayer_throughputUserOnlyAnno15fps.sort_values(by=['fileSize'])


UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser30fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserBroadcast)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserBroadcast15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserFrametoAnno)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserFrametoAnno15fps)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserOnlyAnno)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserOnlyAnno15fps)


g=sns.catplot(x='fileSize',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=4)
plt.xlabel("n vehicles")
g.set_xticklabels(rotation=80)
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndFileSizeX.png',bbox_inches='tight')
