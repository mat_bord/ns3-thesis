import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
from statistics import mean
from IPython.display import display


sns.set_style("white")

parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='PlotComparison/'



#30 fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyD30fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyD30fps = campaign.get_results_as_dataframe(get_latencyD30fps, verbose=True )
display(avg_latencyD30fps)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyC30fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyC30fps= campaign.get_results_as_dataframe(get_latencyC30fps,verbose=True )
display(avg_latencyC30fps)

totLatency30fps=pd.DataFrame()
totLatency30fps=totLatency30fps.append(avg_latencyD30fps)
totLatency30fps['average latency [ms]']=totLatency30fps['average latency [ms]']+avg_latencyC30fps['average latency [ms]']

#15 fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyD15fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyD15fps = campaign.get_results_as_dataframe(get_latencyD15fps, verbose=True )
display(avg_latencyD15fps)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyC15fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyC15fps= campaign.get_results_as_dataframe(get_latencyC15fps,verbose=True )
display(avg_latencyC15fps)

totLatency15fps=pd.DataFrame()
totLatency15fps=totLatency15fps.append(avg_latencyD15fps)
totLatency15fps['average latency [ms]']=totLatency15fps['average latency [ms]']+avg_latencyC15fps['average latency [ms]']



#Broadcast 30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDBroadcast(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]


    # Use the parsing function to create a Pandas dataframe
avg_latencyDBroadcast = campaign.get_results_as_dataframe(get_latencyDBroadcast, verbose=True )
display(avg_latencyDBroadcast)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCBroadcast(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE when is different by zero because the zero could change the average if we put it in the array
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCBroadcast= campaign.get_results_as_dataframe(get_latencyCBroadcast,verbose=True )
display(avg_latencyCBroadcast)
totLatencyBroadcast=pd.DataFrame()
totLatencyBroadcast=totLatencyBroadcast.append(avg_latencyDBroadcast)
totLatencyBroadcast['average latency [ms]']=totLatencyBroadcast['average latency [ms]']+avg_latencyCBroadcast['average latency [ms]']




#Broadcast 15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDBroadcast15fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]


    # Use the parsing function to create a Pandas dataframe
avg_latencyDBroadcast15fps = campaign.get_results_as_dataframe(get_latencyDBroadcast15fps, verbose=True )
display(avg_latencyDBroadcast15fps)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCBroadcast15fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE when is different by zero because the zero could change the average if we put it in the array
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCBroadcast15fps= campaign.get_results_as_dataframe(get_latencyCBroadcast15fps,verbose=True )
display(avg_latencyCBroadcast15fps)
totLatencyBroadcast15fps=pd.DataFrame()
totLatencyBroadcast15fps=totLatencyBroadcast15fps.append(avg_latencyDBroadcast)
totLatencyBroadcast15fps['average latency [ms]']=totLatencyBroadcast15fps['average latency [ms]']+avg_latencyCBroadcast15fps['average latency [ms]']



#FrameToAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#AVG latency Drone-BS vs BS-Car
#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDfTr(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDFrameToAnno = campaign.get_results_as_dataframe(get_latencyDfTr, verbose=True )
display(avg_latencyDFrameToAnno)

#Drop the useless column
avg_latencyDFrameToAnno = avg_latencyDFrameToAnno.drop('fileSizeAnno', 1)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCfTr(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCFrameToAnno= campaign.get_results_as_dataframe(get_latencyCfTr,verbose=True )
display(avg_latencyCFrameToAnno)

#Drop the useless column
avg_latencyCFrameToAnno = avg_latencyCFrameToAnno.drop('fileSizeAnno', 1)


#latency end to end
totLatencyFrameToAnno=pd.DataFrame()
totLatencyFrameToAnno=totLatencyFrameToAnno.append(avg_latencyDFrameToAnno)
totLatencyFrameToAnno['average latency [ms]']=totLatencyFrameToAnno['average latency [ms]']+avg_latencyCFrameToAnno['average latency [ms]']


#FrameToAnno15fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#AVG latency Drone-BS vs BS-Car
#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDfTr15fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDFrameToAnno15fps = campaign.get_results_as_dataframe(get_latencyDfTr15fps, verbose=True )
display(avg_latencyDFrameToAnno15fps)

#Drop the useless column
avg_latencyDFrameToAnno15fps = avg_latencyDFrameToAnno15fps.drop('fileSizeAnno', 1)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCfTr15fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCFrameToAnno15fps= campaign.get_results_as_dataframe(get_latencyCfTr15fps,verbose=True )
display(avg_latencyCFrameToAnno15fps)

#Drop the useless column
avg_latencyCFrameToAnno15fps = avg_latencyCFrameToAnno15fps.drop('fileSizeAnno', 1)


#latency end to end
totLatencyFrameToAnno15fps=pd.DataFrame()
totLatencyFrameToAnno15fps=totLatencyFrameToAnno15fps.append(avg_latencyDFrameToAnno15fps)
totLatencyFrameToAnno15fps['average latency [ms]']=totLatencyFrameToAnno15fps['average latency [ms]']+avg_latencyCFrameToAnno15fps['average latency [ms]']



#onlyAnno30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDOnlyAnno(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDOnlyAnno = campaign.get_results_as_dataframe(get_latencyDOnlyAnno, verbose=True )
display(avg_latencyDOnlyAnno)


#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCOnlyAnno(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCOnlyAnno= campaign.get_results_as_dataframe(get_latencyCOnlyAnno,verbose=True )


#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
avg_latencyCOnlyAnno=avg_latencyCOnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in avg_latencyCOnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    avg_latencyCOnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(avg_latencyCOnlyAnno)

#latency end to end
totLatencyOnlyAnno=pd.DataFrame()
totLatencyOnlyAnno=totLatencyOnlyAnno.append(avg_latencyCOnlyAnno)
totLatencyOnlyAnno['average latency [ms]']=totLatencyOnlyAnno['average latency [ms]']+avg_latencyDOnlyAnno['average latency [ms]']


#onlyAnno150fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDOnlyAnno15fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDOnlyAnno15fps = campaign.get_results_as_dataframe(get_latencyDOnlyAnno15fps, verbose=True )
display(avg_latencyDOnlyAnno15fps)


#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCOnlyAnno15fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCOnlyAnno15fps= campaign.get_results_as_dataframe(get_latencyCOnlyAnno15fps,verbose=True )


#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
avg_latencyCOnlyAnno15fps=avg_latencyCOnlyAnno15fps.sort_values(by=['numUe'])
rngrun=0
for index, row in avg_latencyCOnlyAnno15fps.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    avg_latencyCOnlyAnno15fps.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(avg_latencyCOnlyAnno15fps)

#latency end to end
totLatencyOnlyAnno15fps=pd.DataFrame()
totLatencyOnlyAnno15fps=totLatencyOnlyAnno15fps.append(avg_latencyCOnlyAnno15fps)
totLatencyOnlyAnno15fps['average latency [ms]']=totLatencyOnlyAnno15fps['average latency [ms]']+avg_latencyDOnlyAnno15fps['average latency [ms]']

#User X axes Plot

#Multiple latnecy into 1 plot
Latency_allinone=pd.DataFrame()
totLatency30fps['Type'] = 'Scenario1'
totLatency15fps['Type'] = 'Scenario1_15fps'
totLatencyBroadcast['Type'] = 'Scenario2'
totLatencyBroadcast15fps['Type']= 'Scenario2_15fps'
totLatencyFrameToAnno['Type'] = 'Scenario3'
totLatencyFrameToAnno15fps['Type'] = 'Scenario3_15fps'
totLatencyOnlyAnno['Type'] = 'Scenario4'
totLatencyOnlyAnno15fps['Type'] = 'Scenario4_15fps'
Latency_allinone=Latency_allinone.append(totLatency30fps)
Latency_allinone=Latency_allinone.append(totLatency15fps)
Latency_allinone=Latency_allinone.append(totLatencyBroadcast)
Latency_allinone=Latency_allinone.append(totLatencyBroadcast15fps)
Latency_allinone=Latency_allinone.append(totLatencyFrameToAnno)
Latency_allinone=Latency_allinone.append(totLatencyFrameToAnno15fps)
Latency_allinone=Latency_allinone.append(totLatencyOnlyAnno)
Latency_allinone=Latency_allinone.append(totLatencyOnlyAnno15fps)


sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX.png',bbox_inches='tight')

#FileSize X axes Plot
#Multiple latnecy into 1 plot
Latency_allinone=pd.DataFrame()
avg_latency30fps=totLatency30fps.sort_values(by=['fileSize'])
avg_latency15fps=totLatency15fps.sort_values(by=['fileSize'])
avg_latencyBroadcast=totLatencyBroadcast.sort_values(by=['fileSize'])
avg_latencyBroadcast15fps=totLatencyBroadcast15fps.sort_values(by=['fileSize'])
totLatencyFrameToAnno=totLatencyFrameToAnno.sort_values(by=['fileSize'])
totLatencyFrameToAnno15fps=totLatencyFrameToAnno15fps.sort_values(by=['fileSize'])
avg_latencyOnlyAnno=totLatencyOnlyAnno.sort_values(by=['fileSize'])
avg_latencyOnlyAnno15fps=totLatencyOnlyAnno15fps.sort_values(by=['fileSize'])


Latency_allinone=Latency_allinone.append(avg_latency30fps)
Latency_allinone=Latency_allinone.append(avg_latency15fps)
Latency_allinone=Latency_allinone.append(avg_latencyBroadcast)
Latency_allinone=Latency_allinone.append(avg_latencyBroadcast15fps)
Latency_allinone=Latency_allinone.append(totLatencyFrameToAnno)
Latency_allinone=Latency_allinone.append(totLatencyFrameToAnno15fps)
Latency_allinone=Latency_allinone.append(avg_latencyOnlyAnno)
Latency_allinone=Latency_allinone.append(avg_latencyOnlyAnno15fps)


g=sns.catplot(data=Latency_allinone,
            x='fileSize',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=4)

g.set_xticklabels(rotation=80)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndFileSizeX.png',bbox_inches='tight')
