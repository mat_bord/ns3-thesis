/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


// Network Topology eg.5 cars
//                       car  car
//                        |    |
//        PtP-mmwave      |    |
// drone -------------- BaseStation -- car
//                        |     |  
//                        |     | 
//                       car    car

//NS_GLOBAL_VALUE="RngRun=<number>" ./waf --run DroneBSCar

#include "ns3/mmwave-helper.h"
#include "ns3/epc-helper.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/config-store.h"
#include "ns3/mmwave-point-to-point-epc-helper.h"
//#include "ns3/gtk-config-store.h"


using namespace ns3;
using namespace mmwave;
NS_LOG_COMPONENT_DEFINE ("DroneBSCar");
int main (int argc, char *argv[]) {
  //seed to create the random variable
  //ns3::RngSeedManager::SetSeed(3);
  uint16_t numUe = 2;
  double simTime = 0.5;
  double fileSize=0; //byte 
  
 // Command line arguments
  CommandLine cmd;
  cmd.AddValue ("numUe", "Number of UEs per eNB", numUe);
  cmd.AddValue ("simTime", "Total duration of the simulation [s])", simTime);
  cmd.AddValue ("fileSize", "Total size of the frame [byte])", fileSize);
  cmd.Parse (argc, argv);

  LogComponentEnable ("UdpClient",LOG_LEVEL_INFO); 
  LogComponentEnable ("PacketSink", LOG_LEVEL_INFO); 
  LogComponentEnable ("NetDevice", LOG_LEVEL_INFO); 
  //LogComponentEnable ("MmWaveEnbMac", LOG_LEVEL_INFO); 
  //LogComponentEnable ("MmWaveUeMac", LOG_LEVEL_INFO); 
  //LogComponentEnable ("LteEnbRrc", LOG_LEVEL_INFO); 
  //LogComponentEnable ("LteUeRrc", LOG_LEVEL_INFO); 
  //LogComponentEnable ("LteRlcAm", LOG_LEVEL_INFO); 
  //LogComponentEnable ("LteRlcUm", LOG_LEVEL_INFO); 
  //LogComponentEnable ("LteRlcUmLowLat", LOG_LEVEL_INFO); 
  //LogComponentEnable ("MmWaveFlexTtiMacScheduler", LOG_LEVEL_LOGIC);
  /*LogComponentEnable ("NetDevice", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("NetDeviceQueueInterface", LOG_LEVEL_LOGIC);
  LogComponentEnable ("Ipv4Header", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4PacketInfoTag", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4AddressHelper", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4EndPoint", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4EndPointDemux", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4FlowProbe", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4GlobalRouting", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4Header", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4Interface", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4InterfaceAddress", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4PacketFilter", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4StaticRouting", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4RoutingProtocol", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4RoutingTableEntry", LOG_LEVEL_LOGIC); 
  LogComponentEnable ("Ipv4RawSocketFactory", LOG_LEVEL_LOGIC); */

  LogComponentEnableAll (LOG_PREFIX_TIME); 
  LogComponentEnableAll (LOG_PREFIX_FUNC); 
  LogComponentEnableAll (LOG_PREFIX_NODE);


  Config::SetDefault ("ns3::LteRlcAm::ReportBufferStatusTimer", TimeValue (MicroSeconds (100.0)));
  Config::SetDefault ("ns3::LteRlcUmLowLat::ReportBufferStatusTimer", TimeValue (MicroSeconds (100.0)));
  Config::SetDefault ("ns3::LteRlcUm::MaxTxBufferSize", UintegerValue (10*1024*1024));//Change the RLCUM buffer size
  Config::SetDefault ("ns3::LteRlcAm::MaxTxBufferSize", UintegerValue (10*1024*1024));//Change the RLCAM buffer size

  std::cout << "start"<< std::endl;

  Ptr<MmWaveHelper> mmwaveHelper = CreateObject<MmWaveHelper> ();
  Ptr<MmWavePointToPointEpcHelper>  epcHelper = CreateObject<MmWavePointToPointEpcHelper> ();
  mmwaveHelper->SetEpcHelper (epcHelper);

  ConfigStore inputConfig;Config::SetDefault ("ns3::LteRlcAm::MaxTxBufferSize", UintegerValue (10*1024*1024));
  inputConfig.ConfigureDefaults();
  cmd.Parse (argc, argv);

  InternetStackHelper internet;
  
 //create two nodes that we will connect via the point-to-point link
  NodeContainer enbNodes;
  NodeContainer ueDrone;
  NodeContainer ueNodes;
  enbNodes.Create (1);
  ueDrone.Create (1);
  ueNodes.Create (numUe);

  //--------------------------------------------------------  SET THE POSITION FOR EACH NODE
  std::cout << "Starting setting position"<< std::endl;

    //eNB position
  //Define a position at 0,0,0 and assign it to enbNodes
  Ptr<ListPositionAllocator> enbPositionAlloc = CreateObject<ListPositionAllocator> ();
  enbPositionAlloc->Add (Vector (8.0, 8.0, 0.0));
  MobilityHelper enbmobility;
  enbmobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  enbmobility.SetPositionAllocator (enbPositionAlloc);
  enbmobility.Install (enbNodes);

   //UEs position
  MobilityHelper uemobility;
  uemobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  //position for the drone
  Ptr<ListPositionAllocator> ueDronePositionAlloc = CreateObject<ListPositionAllocator> ();
  ueDronePositionAlloc->Add (Vector (0.0, 0.0, 80.0));
  //assign drone position
  uemobility.SetPositionAllocator (ueDronePositionAlloc);
  uemobility.Install (ueDrone);

  //position for vehicles
  uemobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
			                          "X", StringValue ("ns3::UniformRandomVariable[Min=0|Max=40.0]"),  
			                          "Y", StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=22.5]"));
  // each object will be attached a static position.
  // i.e., once set by the "position allocator", the
  // position will never change.
  uemobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  //assign veichle position
  uemobility.Install (ueNodes);

  for (NodeContainer::Iterator j = ueNodes.Begin (); j != ueNodes.End (); ++j)
  {
    Ptr<Node> object = *j;
    Ptr<MobilityModel> position = object->GetObject<MobilityModel> ();
    NS_ASSERT (position != 0);
    Vector pos = position->GetPosition ();
    std::cout << "x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << std::endl;
  }

  std::cout << "Position done"<< std::endl;


//----------------------------------------------------- CONNECT UEs TO THE  eNB
std::cout << "Starting connection between UEs and eNB"<< std::endl;

//This helper enables pcap and ascii tracing of events in the internet stack associated with a node.

  internet.Install (ueDrone);
  internet.Install (ueNodes);

//Install a mmWave protocol stack on the UE drone
  NetDeviceContainer ueDevsDrone;
  ueDevsDrone= mmwaveHelper->InstallUeDevice(ueDrone);

//Install a mmWave protocol stack on the eNB
  NetDeviceContainer enbDevs;
  enbDevs= mmwaveHelper->InstallEnbDevice(enbNodes);

//Install a mmWave protocol stack on the ueS
  NetDeviceContainer ueDevs;
  ueDevs= mmwaveHelper->InstallUeDevice(ueNodes);

  std::cout << "Connection done"<< std::endl;

//------------------------------    CONFIGURE THE UEs AND DRONE FOR IP NETWORKING
std::cout << "Starting configuration IP"<< std::endl;

    //configure drone
  // Install the IP stack on the drone
  Ipv4InterfaceContainer ueIpIfacedrone;
  //Ptr<NetDevice> dronemmwaveDevice = ueDevsDrone.Get (0);
  ueIpIfacedrone = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevsDrone));


      //configue UEs
  // Assign IP address to UEs, and install applications
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevs));


  //Attach the UEs to an eNB. 
  //This will configure each UE according to the eNB configuration, and create an RRC connection between them
  mmwaveHelper->AttachToClosestEnb (ueDevsDrone, enbDevs);
  mmwaveHelper->AttachToClosestEnb (ueDevs, enbDevs);

  std::cout << "IP configuration done"<< std::endl;

//-------------------------------------------------- INSTALL AND START APPLICATIONS ON REMOTE HOST
  std::cout << "Starting installation application"<< std::endl;

  // Install and start applications on UEs
  uint16_t Port = 3000; 
  ApplicationContainer clientApps;
  ApplicationContainer serverApps;
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      //++Port;
      //instantiates a PacketSinkHelper and tells it to create sockets using the class ns3::UdpSocketFactory
      //remaining parameter tells the Application which address and port it should Bind to
      PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (ueIpIface.GetAddress (u), Port));
      serverApps.Add (packetSinkHelper.Install (ueNodes.Get(u)));
    }

    //Create a client application which sends UDP packets
    UdpClientHelper client (ueIpIface.GetAddress (ueNodes.GetN ()-1), Port);//IP and port of the server

    double interval= 0.033; //interval to send pckts
    
    if (fileSize<13){
      fileSize=13;
    }

    client.SetAttribute ("Interval", TimeValue (Seconds(interval)));//
    client.SetAttribute ("MaxPackets", UintegerValue (4294967295));//Set the maximum amount of pckts to send= N pckts each second * N UE * simulation time
    client.SetAttribute ("PacketSize", UintegerValue (fileSize));//specify the packet size: MTU ethernet 1500B so if in 1 sec I want to send 30pcks=105MB
                                                              // I have to send 70000 small pckts of 1500B to each UE

    clientApps=client.Install (ueDrone.Get(0));



  std::cout << "Application installed"<< std::endl;


//---------------------------------------------
  std::cout << "Starting simulation"<< std::endl;

  serverApps.Start (Seconds (0.0));
  clientApps.Start (Seconds (0.0));

  mmwaveHelper->EnableTraces ();


  Simulator::Stop (Seconds (simTime));
  Simulator::Run ();

double throughput = 0;
  for (uint32_t index = 0; index < serverApps.GetN (); ++index)
    {
      uint64_t totalPacketsThrough = DynamicCast<PacketSink> (serverApps.Get (index))->GetTotalRx ();
      throughput += ((totalPacketsThrough * 8) / (simTime * 1000000.0)); //Mbit/s
    }

  Simulator::Destroy ();

  if (throughput > 0)
    {
      std::cout << "Aggregated throughput: " << throughput << " Mbit/s" << std::endl;
    }
  else
    {
      NS_LOG_ERROR ("Obtained throughput is 0!");
      exit (1);
    }

  return 0;



}
