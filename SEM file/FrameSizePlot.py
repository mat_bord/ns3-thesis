import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
import scipy.stats as st
from IPython.display import display
import itertools
from decimal import Decimal
from statistics import mean


sns.set_style("white")

plotDir='PlotComparison/'
ns_path = '../../../../nfsd/signet4/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet4/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet4/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)


numUE=[]
fileSize=[]
plot=pd.DataFrame()
for i in range(0,len(parameters),2):
    numUE.append(parameters[i])
    fileSize.append(parameters[i+1]) # I multiply BB size* n UE inside the frame * fps * ...
plot['numUe']=numUE
plot['fileSize Byte']=fileSize

display(plot)

sns.catplot(data=plot,
            x='numUe',
            y='fileSize Byte',
            kind='point',
            height=5, aspect=2)


plt.grid()
plt.title('Frame Size')
plt.savefig(plotDir+'FrameSize.png',bbox_inches='tight')
