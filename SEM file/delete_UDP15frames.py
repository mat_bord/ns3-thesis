import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
import scipy.stats as st
from IPython.display import display
import itertools
from decimal import Decimal
from statistics import mean


sns.set_style("white")

ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

deleteUE=[19, 20, 31, 32, 33, 34]

print('Before:')
for result in campaign.db.get_complete_results():
    print(result['params']['numUe'])

for result in campaign.db.get_complete_results():
    for ue in deleteUE:
        if result['params']['numUe'] == ue:
            campaign.db.delete_result(result)

print('After:')
for result in campaign.db.get_complete_results():
    print(result['params']['numUe'])