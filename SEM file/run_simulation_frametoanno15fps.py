import sys
import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
#import cv2
sns.set_style("white")
os.system("pwd")
parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")

for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)


ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis2'
script = 'FrameToAnnoDroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames15fps"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False, max_parallel_processes=64)

print('Tot frames '+str(len(parameters)/2))

paramstot=[]

for i in range(0,len(parameters),2):
    params = {
        'numUe': parameters[i], #change with the variable listUE
        'simTime': 15, 
        'fileSize':parameters[i+1], #it is changing the fie size every simulation #e.g 225000 byte = 225 kb 
        'fileSizeAnno':39.7*parameters[i], #one bounding box has size of 39.7 byte so I multiply it by the number of UEs in the scene
        "RngRun": list(range(1,9))
        }
    paramstot.append(params)
    

campaign.run_missing_simulations(paramstot)
