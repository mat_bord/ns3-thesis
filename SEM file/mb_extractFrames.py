#VIDEO NAME AS ONLY PARAMETER OF INPUT


import numpy as np
import os
import shutil
import pandas as pd
import cv2
import sys


path_to_folders='../../Downloads/'


def extractAllFrames(video):
  short_video=video.split(".")[0]
  video_path=path_to_folders+'Video/'+video
  vidcap = cv2.VideoCapture(video_path)
  success,image = vidcap.read()
  frame = 0
  print(int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT)))
  print("Video "+str(video))
  while success:
    cv2.imwrite("frames/"+short_video+"_%d.jpg" % frame, image)     # save frame as JPEG file      
    success,image = vidcap.read()
    frame += 1    



#VIDEO NAME AS ONLY PARAMETER OF INPUT
video=sys.argv[1]
os.system('mkdir frames')
os.system('mkdir all_labels_groundtruth')
object_final = {'Pedestrian': 0, 'Biker': 1, 'Skater': 0, 'Cart': 2, 'Car': 2, 'Bus': 2}
#extract all the frames
extractAllFrames(video)
