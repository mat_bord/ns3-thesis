
import numpy as np
import os
import shutil
import pandas as pd
import cv2
import sys


path_to_folders='../../Downloads/'

def centerBB(row, heightIm, widthIm):
  x=((row['xmax']+row['xmin'])/2 )/widthIm
  y=((row['ymax']+row['ymin'])/2 )/heightIm
  return str(x),str(y)

#function to get width and height of the BB
def dimBB(row, heightIm, widthIm):
    widthBB=(row['xmax']-row['xmin'])/widthIm
    heightBB=(row['ymax']-row['ymin'])/heightIm
    return str(widthBB), str(heightBB)

#Annotation format: class_index bbox_x_center bbox_y_center bbox_width bbox_height
def extract_annotations(annotation, data_type, short_annotation ):
    print("Label "+str(annotation))
    df_final = pd.read_csv(path_to_folders+'Labels/'+annotation, sep=' ', engine='python', header=None, names=['ID','xmin','ymin','xmax','ymax','frame','lost','occluded','generated','label'])
    #df_final = df_final[df_final.occluded != 1]
    df_final = df_final[df_final.lost != 1]
    df_final.sort_values('frame',inplace=True)#order by frame
    #get video information
    file_path = path_to_folders+"Video/"+annotation.split(".")[0]+".mov"
    vid = cv2.VideoCapture(file_path)
    heightIm = vid.get(cv2.CAP_PROP_FRAME_HEIGHT)
    widthIm = vid.get(cv2.CAP_PROP_FRAME_WIDTH)
    totr= df_final.shape[0] #total number of rows
    i=0 #actual index in the dataframe
    for _, row in df_final.iterrows():
      xc,yc=centerBB(row, heightIm, widthIm)
      widthBB, heightBB=dimBB(row, heightIm, widthIm)
      #open again the file only if it was closed or is the first one
      if(i==0 or df_final.iloc[i-1,5]!=row['frame']):
          file = open(data_type+"/"+short_annotation+"_%d.txt" % row['frame'], "w") 
      file.write(str(object_final.get(row['label'])) + " " + xc + " " + yc+ " " + widthBB+ " " + heightBB ) 
      #if the next frame in the dataframe is different close the file otherwise create new line to add new coordinates
      if(i+1==totr or df_final.iloc[i+1,5] !=row['frame']): 
          file.close()
      else:
          file.write("\n")
      #print("Read a new frame n."+str(row['frame'])+" of video "+ str(short_annotation))
      i=i+1



#VIDEO NAME AS ONLY PARAMETER OF INPUT
video=sys.argv[1]
object_final = {'Pedestrian': 0, 'Biker': 1, 'Skater': 0, 'Cart': 2, 'Car': 2, 'Bus': 2}
path_totlabels = 'all_labels_groundtruth/'

cap = cv2.VideoCapture(path_to_folders+'Video/'+video)
print(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))
x=video.split(".")
nameanno=x[0]+".txt"
#extract all the labels divided by frame
extract_annotations(nameanno, path_totlabels, x[0])
