import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
import scipy.stats as st
from IPython.display import display
import itertools
from decimal import Decimal
from statistics import mean


sns.set_style("white")

plotDir='PlotUDPFrameToAnno/'
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)


parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)

#Pckt received from drone / pckt I want to send
#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_DB_broadcast(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #print(tot_pckt_to_send)
    #calculate pckt recevided from BS
    tot_packet_received=0
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]==str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])
    #print(tot_packet_received)
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    if (pckt_reliability_DB>100):
        pckt_reliability_DB=100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_DBS_broadcast = campaign.get_results_as_dataframe(pckt_reliability_DB_broadcast,verbose=True)
display(prob_pckt_DBS_broadcast)


#Pckt received from UEs / pckt send from BS --- Broadcast

@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_BU(result):
    #calculate total pckt to send from BS
    pcktsintoFrame=result['params']['fileSize']/1500
    tot_packet_to_send=0
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]==str(1) and linesplit[17]==str(1530)):
            tot_packet_to_send=tot_packet_to_send+int(linesplit[8])
    #Since is sended one anno for every time an entire frame arrive (frame is composed by multiple smaller pckts of 1500)
    tot_packet_to_send=tot_packet_to_send/pcktsintoFrame
    #calculate total pckt to send from BS
    tot_packet_to_receive=0
    maxue=0
    annosize=str((result['params']['numUe']*39.7)+30)
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[3]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_to_receive=tot_packet_to_receive+int(linesplit[8])   
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
                
    tot_packet_to_send=tot_packet_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    #print(tot_packet_received)
    pckt_reliability_BU=(tot_packet_to_receive/tot_packet_to_send)*100
    return [pckt_reliability_BU]


# Use the parsing function to create a Pandas dataframe
prob_pckt_BSUE = campaign.get_results_as_dataframe(pckt_reliability_BU, verbose=True )
display(prob_pckt_BSUE)

#reliability Multiple into 1 plot
prob_pckt_DBS_broadcast['Type'] = 'Drone-BS'
prob_pckt_BSUE['Type'] = 'BS-Car'
prob_pckt_DBS_broadcast=prob_pckt_DBS_broadcast.append(prob_pckt_BSUE)


sns.catplot(data=prob_pckt_DBS_broadcast,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received/tot pckt to send Drone-BS vs BS-Car')
plt.savefig(plotDir+'Reliability_pckt_count.png',bbox_inches='tight')