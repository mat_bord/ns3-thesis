import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
from statistics import mean
from IPython.display import display


sns.set_style("white")

parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='thesisPlot/'


#perfect30fps
fileSize=[]
throughput=[]
numue=[]
perfect_throughput30fps=pd.DataFrame()
for i in range(0,len(parameters),2):
    fileSize.append(parameters[i+1])
    throughput.append(((parameters[i+1]+30)*30*8)/1000000) # I multiply frame size * fps * ...
    numue.append(parameters[i])
perfect_throughput30fps['fileSize']=fileSize
perfect_throughput30fps['Throughput [Mbit/s]']=throughput
perfect_throughput30fps['numUe']=numue

display(perfect_throughput30fps)

#30 FPS
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#2nd link throughput per user

#Instead of doing the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUser30fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

# Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUser30fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUser30fps,
                                            throughputPDCP_labelsUser)
display(PDCPlayer_throughputUser30fps)

#15 FPS
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUser15fps(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUser15fps = campaign.get_results_as_dataframe(get_pdcplayerthroughputUser15fps,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUser15fps)

#Total perfect throughput 1st link vs total throughput 1st link
#and 
#perfect throughput per user 1st link vs 1st link throughput per user
#valid for:
# broadcast frame link 1 and 2 as Perfect throughput per user
# broadcast frame link 1 as Total Perfect throughput

fileSize=[]
throughput=[]
perfect_throughput15fps=pd.DataFrame()
numue=[]
for i in range(0,len(parameters),2):
    fileSize.append(parameters[i+1])
    throughput.append(((parameters[i+1]+30)*15*8)/1000000) # I multiply frame size * fps * ...
    numue.append(parameters[i])

perfect_throughput15fps['fileSize']=fileSize
perfect_throughput15fps['Throughput [Mbit/s]']=throughput
perfect_throughput15fps['numUe']=numue
display(perfect_throughput15fps)

#Broadcast
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserBroadcast(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserBroadcast = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserBroadcast,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUserBroadcast)


#FrameToAnno
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserFtA(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserFrametoAnno = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserFtA,throughputPDCP_labelsUser)

#Drop the useless column
PDCPlayer_throughputUserFrametoAnno = PDCPlayer_throughputUserFrametoAnno.drop('fileSizeAnno', 1)


display(PDCPlayer_throughputUserFrametoAnno)


#OnlyAnno 
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUserOnlyAnno(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUserOnlyAnno = campaign.get_results_as_dataframe(get_pdcplayerthroughputUserOnlyAnno,throughputPDCP_labelsUser)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
PDCPlayer_throughputUserOnlyAnno=PDCPlayer_throughputUserOnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in PDCPlayer_throughputUserOnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    PDCPlayer_throughputUserOnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(PDCPlayer_throughputUserOnlyAnno)




#User X axes Plot
UserBC_throughput=pd.DataFrame()

PDCPlayer_throughputUser30fps['Type'] = 'Scenario 1'
PDCPlayer_throughputUser15fps['Type'] = 'Scenario 2'
PDCPlayer_throughputUserBroadcast['Type'] = 'Scenario 3'
PDCPlayer_throughputUserFrametoAnno['Type'] = 'Scenario 4'
PDCPlayer_throughputUserOnlyAnno['Type'] = 'Scenario 5'



UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser30fps)

sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX1.png',bbox_inches='tight')


UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser15fps)

sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX2.png',bbox_inches='tight')

UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserBroadcast)

sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX3.png',bbox_inches='tight')

UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserFrametoAnno)
sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX4.png',bbox_inches='tight')

UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUserOnlyAnno)

sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Throughput per user end to end [Mbit/s]')
plt.savefig(plotDir+'ThroughputEndToEndUSerX5.png',bbox_inches='tight')

###################################################################################################################################

#latency
parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='thesisPlot/'

#30 fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyD30fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyD30fps = campaign.get_results_as_dataframe(get_latencyD30fps, verbose=True )
display(avg_latencyD30fps)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyC30fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyC30fps= campaign.get_results_as_dataframe(get_latencyC30fps,verbose=True )
display(avg_latencyC30fps)

totLatency30fps=pd.DataFrame()
totLatency30fps=totLatency30fps.append(avg_latencyD30fps)
totLatency30fps['average latency [ms]']=totLatency30fps['average latency [ms]']+avg_latencyC30fps['average latency [ms]']

#15 fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyD15fps(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyD15fps = campaign.get_results_as_dataframe(get_latencyD15fps, verbose=True )
display(avg_latencyD15fps)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyC15fps(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyC15fps= campaign.get_results_as_dataframe(get_latencyC15fps,verbose=True )
display(avg_latencyC15fps)

totLatency15fps=pd.DataFrame()
totLatency15fps=totLatency15fps.append(avg_latencyD15fps)
totLatency15fps['average latency [ms]']=totLatency15fps['average latency [ms]']+avg_latencyC15fps['average latency [ms]']



#Broadcast
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDBroadcast(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]


    # Use the parsing function to create a Pandas dataframe
avg_latencyDBroadcast = campaign.get_results_as_dataframe(get_latencyDBroadcast, verbose=True )
display(avg_latencyDBroadcast)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCBroadcast(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE when is different by zero because the zero could change the average if we put it in the array
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCBroadcast= campaign.get_results_as_dataframe(get_latencyCBroadcast,verbose=True )
display(avg_latencyCBroadcast)
totLatencyBroadcast=pd.DataFrame()
totLatencyBroadcast=totLatencyBroadcast.append(avg_latencyDBroadcast)
totLatencyBroadcast['average latency [ms]']=totLatencyBroadcast['average latency [ms]']+avg_latencyCBroadcast['average latency [ms]']

#FrameToAnno
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#AVG latency Drone-BS vs BS-Car
#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDfTr(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDFrameToAnno = campaign.get_results_as_dataframe(get_latencyDfTr, verbose=True )
display(avg_latencyDFrameToAnno)

#Drop the useless column
avg_latencyDFrameToAnno = avg_latencyDFrameToAnno.drop('fileSizeAnno', 1)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCfTr(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCFrameToAnno= campaign.get_results_as_dataframe(get_latencyCfTr,verbose=True )
display(avg_latencyCFrameToAnno)

#Drop the useless column
avg_latencyCFrameToAnno = avg_latencyCFrameToAnno.drop('fileSizeAnno', 1)


#latency end to end
totLatencyFrameToAnno=pd.DataFrame()
totLatencyFrameToAnno=totLatencyFrameToAnno.append(avg_latencyDFrameToAnno)
totLatencyFrameToAnno['average latency [ms]']=totLatencyFrameToAnno['average latency [ms]']+avg_latencyCFrameToAnno['average latency [ms]']


#onlyAnno
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyDOnlyAnno(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyDOnlyAnno = campaign.get_results_as_dataframe(get_latencyDOnlyAnno, verbose=True )
display(avg_latencyDOnlyAnno)


#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyCOnlyAnno(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyCOnlyAnno= campaign.get_results_as_dataframe(get_latencyCOnlyAnno,verbose=True )


#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
avg_latencyCOnlyAnno=avg_latencyCOnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in avg_latencyCOnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    avg_latencyCOnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(avg_latencyCOnlyAnno)

#latency end to end
totLatencyOnlyAnno=pd.DataFrame()
totLatencyOnlyAnno=totLatencyOnlyAnno.append(avg_latencyCOnlyAnno)
totLatencyOnlyAnno['average latency [ms]']=totLatencyOnlyAnno['average latency [ms]']+avg_latencyDOnlyAnno['average latency [ms]']

#User X axes Plot

#Multiple latnecy into 1 plot
Latency_allinone=pd.DataFrame()
totLatency30fps['Type'] = 'Scenario 1'
totLatency15fps['Type'] = 'Scenario 2'
totLatencyBroadcast['Type'] = 'Scenario 3'
totLatencyFrameToAnno['Type'] = 'Scenario 4'
totLatencyOnlyAnno['Type'] = 'Scenario 5'


Latency_allinone=Latency_allinone.append(totLatency30fps)

sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX1.png',bbox_inches='tight')

Latency_allinone=Latency_allinone.append(totLatency15fps)

sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX2.png',bbox_inches='tight')

Latency_allinone=Latency_allinone.append(totLatencyBroadcast)

sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX3.png',bbox_inches='tight')

Latency_allinone=Latency_allinone.append(totLatencyFrameToAnno)

sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX4.png',bbox_inches='tight')

Latency_allinone=Latency_allinone.append(totLatencyOnlyAnno)

sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms]  end to end')
plt.savefig(plotDir+'avg_latencyEndToEndUSerX5.png',bbox_inches='tight')



######################################################################################################################################

#reliability
parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)
plotDir='thesisPlot/'


#30fps
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)
#Pckt received from drone / pckt I want to send - 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_30fps(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15*result['params']['numUe']
    #calculate total pckt to UEs
    tot_packet_received=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_30fps = campaign.get_results_as_dataframe(pckt_reliability_30fps, verbose=True )
display(prob_pckt_30fps)


#15fps

ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'DroneBSCar15fps'
campaign_dir = "../../../../nfsd/signet3/bordinm/UDP15Frames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - 15 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_15fps(result):
    byteS   = result['params']['fileSize']*15; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15*result['params']['numUe']#15 is the simulation time
    #calculate total pckt to Ues
    tot_packet_received=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])      
    
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]


# Use the parsing function to create a Pandas dataframe
prob_pckt_15fps = campaign.get_results_as_dataframe(pckt_reliability_15fps,verbose=True )
display(prob_pckt_15fps)


#Broadcast
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'BroadcastDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/BroadcastUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_broadcast(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_broadcast = campaign.get_results_as_dataframe(pckt_reliability_broadcast,verbose=True)
display(prob_pckt_broadcast)

#FrameToAnno
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])

#In this scenario the number of pckts that arrives is not equal to the number of pckts to send: 
# to do a good calculus lets assign to TOT_PACKETS the total number of pckts that have to arrive at the end
def pckt_reliability_fta(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.033; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to UEs
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    return [pckt_reliability_DB]
# Use the parsing function to create a Pandas dataframe
prob_pckt_frameToAnno = campaign.get_results_as_dataframe(pckt_reliability_fta,verbose=True)
display(prob_pckt_frameToAnno)

#Drop the useless column
prob_pckt_frameToAnno = prob_pckt_frameToAnno.drop('fileSizeAnno', 1)


#OnlyAnno
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'OnlyAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/OnlyAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)
#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_OnlyAnno(result):
    annosize=str((result['params']['numUe']*39.7)+30)
    Npckts  = 1/0.033; #how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #calculate total pckt to send to Ues
    tot_packet_received=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_received=tot_packet_received+int(linesplit[8])  
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
                
    tot_pckt_to_send=tot_pckt_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    #print(tot_packet_received)
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    if (pckt_reliability_DB>100):
        pckt_reliability_DB=100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_OnlyAnno = campaign.get_results_as_dataframe(pckt_reliability_OnlyAnno,verbose=True)

#substitute every annosize with the frame size in order to plot it in the frameSize plot
#the file size will be correct because the parameters variable is the same as the one we use to do the simulations
prob_pckt_OnlyAnno=prob_pckt_OnlyAnno.sort_values(by=['numUe'])
rngrun=0
for index, row in prob_pckt_OnlyAnno.iterrows():
    rngrun=rngrun+1
    i=parameters.index(row['numUe'])
    prob_pckt_OnlyAnno.at[index,'fileSize'] = parameters[i+1]
    #I delete the element at position "i" in order to avoid that the next time it will search the same nuber, 
    #the program will choose the same element
    if(rngrun==8):
        del parameters[i]
        #since the list is translated I remove also the associated fileSize
        del parameters[i]
        rngrun=0

#Now in the fileSize I should have all the fileSize of the frames (not of the annotation)

display(prob_pckt_OnlyAnno)

#User X axes Plot
#Multiple latnecy into 1 plot
Reliability_allinone=pd.DataFrame()
prob_pckt_30fps['Type'] = 'Scenario 1'
prob_pckt_15fps['Type'] = 'Scenario 2'
prob_pckt_broadcast['Type'] = 'Scenario 3'
prob_pckt_frameToAnno['Type'] = 'Scenario 4'
prob_pckt_OnlyAnno['Type'] = 'Scenario 5'

Reliability_allinone=Reliability_allinone.append(prob_pckt_30fps)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX1.png',bbox_inches='tight')

Reliability_allinone=Reliability_allinone.append(prob_pckt_15fps)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX2.png',bbox_inches='tight')

Reliability_allinone=Reliability_allinone.append(prob_pckt_broadcast)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX3.png',bbox_inches='tight')

Reliability_allinone=Reliability_allinone.append(prob_pckt_frameToAnno)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX4.png',bbox_inches='tight')

Reliability_allinone=Reliability_allinone.append(prob_pckt_OnlyAnno)

sns.catplot(data=Reliability_allinone,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received UE/tot pckt to send')
plt.savefig(plotDir+'Reliability_pckt_EndToEndUSerX5.png',bbox_inches='tight')



