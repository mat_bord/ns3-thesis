import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
import scipy.stats as st
from IPython.display import display
import itertools
from decimal import Decimal
from statistics import mean


sns.set_style("white")

plotDir='PlotUDPFrameToAnno/'
ns_path = '../../../../nfsd/signet3/bordinm/ns3-thesis'
script = 'FrameToAnnoDroneBSCar'
campaign_dir = "../../../../nfsd/signet3/bordinm/FrameToAnnoUDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)


parameters=[] #[0]= numUE, [1]=fileSize
pathttofolders="../../../../nfsd/signet3/bordinm/"
onlyfiles = os.listdir(pathttofolders+"all_labels_groundtruth/")
for label in onlyfiles:
    df = pd.read_csv(pathttofolders+"all_labels_groundtruth/"+label, delimiter = "\t", header=None)
    nUE=len(df.index)
    #search the file in the frames directory
    x=label.split(".")
    frame=x[0]+".jpg"
    onlyframes = os.listdir(pathttofolders+"frames/")
    if (frame in onlyframes):
        parameters.append(nUE)
        parameters.append(os.stat(pathttofolders+"frames/"+frame).st_size)
print(parameters)


#Total perfect throughput 1st link vs total throughput 1st link
#and 
#perfect throughput per user 1st link vs 1st link throughput per user

#valid for:
# frame to anno link 1 as Perfect throughput per user
# frame to anno link 1 as Total Perfect throughput
fileSize=[]
throughput=[]
perfect_throughput=pd.DataFrame()
numUe=[]
for i in range(0,len(parameters),2):
    fileSize.append(parameters[i+1])
    throughput.append(((parameters[i+1]+30)*30.5*8)/1000000) # I multiply frame size * fps * ...
    numUe.append(parameters[i])
perfect_throughput['numUe']=numUe    
perfect_throughput['fileSize']=fileSize
perfect_throughput['Throughput [Mbit/s]']=throughput

display(perfect_throughput)

#Throughput PDCP Drone-BS 

#Valid for:
#Throughput per user
#Total throughput 

#Info are filtered in this way:
# Ul: get file size of lines with Rx (by base station) and RNTI=1
# Dl: get file size of lines with Rx (by Ues) and RNTI!=1 (all except the drone)
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputD(result):
    Rd=0
    Rc=0
    #calculus throughput drone--BS
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]=='1'):
            Rd+=int(linesplit[9])
    AVGRd=(Rd*8)/(result['params']['simTime']*1000000)
    return [AVGRd]
# Tell SEM how to label the output of the function
throughputPDCP_labelsD = ['Throughput [Mbit/s]']


# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputD = campaign.get_results_as_dataframe(get_pdcplayerthroughputD,throughputPDCP_labelsD )

display(PDCPlayer_throughputD)

#perfect and drone-bs into 1 plot
PDB_throughput=pd.DataFrame()

PDCPlayer_throughputD['Type'] = 'Drone-BS'
perfect_throughput['Type'] = 'Perfect'



PDB_throughput=PDB_throughput.append(PDCPlayer_throughputD)
PDB_throughput=PDB_throughput.append(perfect_throughput)

del PDB_throughput['RngRun']
del PDB_throughput['simTime']

print(PDB_throughput)

g=sns.catplot(data=PDB_throughput,
            x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Drone-BS: Total perfect throughput vs  total throughput [Mbit/s] \n Drone-BS: perfect throughput per user vs throughput per user')
plt.savefig(plotDir+'Drone-BSPerfectthroughputVSthroughputAndperuser.png',bbox_inches='tight')

#1st link total throughput vs 2nd link total throughput
#Throughput PDCP Drone-BS for a VS
#Info are filtered in this way:
# Ul: get file size of lines with Rx (by base station) and RNTI=1
# Dl: get file size of lines with Rx (by Ues) and RNTI!=1 (all except the drone)
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputD(result):
    Rd=0
    Rc=0
    maxue=0
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]=='1'):
            Rd+=int(linesplit[9])
    AVGRd=(Rd*8)/(result['params']['simTime']*1000000)
    #(how many frames arrived)*(the size of an annotation in this simulation)*(how many UEs have to receive it)
    AVGRd=(AVGRd/result['params']['fileSize'])*(39.7*result['params']['numUe'])*(maxue-1)
    return [AVGRd]
# Tell SEM how to label the output of the function
throughputPDCP_labelsD = ['Throughput [Mbit/s]']


# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputD = campaign.get_results_as_dataframe(get_pdcplayerthroughputD,throughputPDCP_labelsD )
PDCPlayer_throughputD=PDCPlayer_throughputD.sort_values(by=['fileSize'])

display(PDCPlayer_throughputD)

#Throughput PDCP BS-Car
#Info are filtered in this way:
# Ul: get file size of lines with Rx (by base station) and RNTI=1
# Dl: get file size of lines with Rx (by Ues) and RNTI!=1 (all except the drone)
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputC(result):
    Rc=0
    #calculus throughput BS--car
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                Rc+=int(linesplit[9])
    AVGRc=(Rc*8)/(result['params']['simTime']*1000000)

    return [AVGRc]

# Tell SEM how to label the output of the function
throughputPDCP_labelsC = ['Throughput [Mbit/s]']

# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputC = campaign.get_results_as_dataframe(get_pdcplayerthroughputC,throughputPDCP_labelsC )
PDCPlayer_throughputC=PDCPlayer_throughputC.sort_values(by=['fileSize'])

display(PDCPlayer_throughputC)

#Drone-BS and BS-car into 1 plot
DBBC_throughput=pd.DataFrame()

PDCPlayer_throughputD['Type'] = 'Drone-BS'
PDCPlayer_throughputC['Type'] = 'BS-Car'


DBBC_throughput=DBBC_throughput.append(PDCPlayer_throughputC)
DBBC_throughput=DBBC_throughput.append(PDCPlayer_throughputD)

display(DBBC_throughput)

g=sns.catplot(x='fileSize',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=DBBC_throughput,
            height=5, aspect=4)
plt.xlabel("n vehicles")
g.set_xticklabels(rotation=80)

plt.grid()
plt.title('Drone-BS total throughput frames vs BS-Car total throughput annotation [Mbit/s]')
plt.savefig(plotDir+'BS-CarthroughputVSDrone-BSthroughput.png',bbox_inches='tight')


#2nd link throughput per user vs 1st link throughput per user 

#Throughput PDCP BS-Car for user

#Instead of dong the sum of the throughput of each user I do the mean
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput PDCP layer [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputUser(result):
    bitPerUser=np.zeros(shape=(result['params']['numUe'] )) 
    print(result['params']['numUe'])
    #calculus throughput BS--car per user
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[0]!="%"):
            if (linesplit[4]!='1'):
                #-2 because the original number 1 is the drone and so every Nue is translated in +1 and also the array is starting from 0 not 1
                bitPerUser[int(linesplit[4])-2]=bitPerUser[int(linesplit[4])-2]+int(linesplit[9])
    nozeroThroughputPerUser=bitPerUser[bitPerUser!= 0]
    throughputPerUser=[]
    for throughput in nozeroThroughputPerUser:
        throughputPerUser.append((throughput*8)/(result['params']['simTime']*1000000))
    AVGUs=mean(throughputPerUser)
    return [AVGUs]

    # Tell SEM how to label the output of the function
throughputPDCP_labelsUser= ['Throughput [Mbit/s]']
# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputUser = campaign.get_results_as_dataframe(get_pdcplayerthroughputUser,throughputPDCP_labelsUser)
display(PDCPlayer_throughputUser)

#Throughput PDCP Drone-BS for a VS for user

#Info are filtered in this way:
# Ul: get file size of lines with Rx (by base station) and RNTI=1
# Dl: get file size of lines with Rx (by Ues) and RNTI!=1 (all except the drone)
@sem.utils.yields_multiple_results
@sem.utils.output_labels(['Throughput [Mbit/s]', 'CI_Diff'])

def get_pdcplayerthroughputD(result):
    Rd=0
    #calculus throughput drone--BS
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]=='1'):
            Rd+=int(linesplit[9])
    #(how many frames arrived at BS)*(the size of an annotation in this simulation)
    Rd=(Rd/result['params']['fileSize']+30)*(39.7*result['params']['numUe'])
    AVGRd=(Rd*8)/(result['params']['simTime']*1000000)
    return [AVGRd]
# Tell SEM how to label the output of the function
throughputPDCP_labelsD = ['Throughput [Mbit/s]']


# Use the parsing function to create a Pandas dataframe
PDCPlayer_throughputD = campaign.get_results_as_dataframe(get_pdcplayerthroughputD,throughputPDCP_labelsD )

display(PDCPlayer_throughputD)

UserBC_throughput=pd.DataFrame()

PDCPlayer_throughputUser['Type'] = 'BS-Car'
PDCPlayer_throughputD['Type'] = 'Drone-BS'
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputD)
UserBC_throughput=UserBC_throughput.append(PDCPlayer_throughputUser)

display(UserBC_throughput)

sns.catplot(x='numUe',
            y='Throughput [Mbit/s]',
            hue='Type',
            kind='point',
            data=UserBC_throughput,
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Drone-BS throughput per user vs BS-Car throughput per user [Mbit/s]')
plt.savefig(plotDir+'Drone-BSthroughputuserVSBS-Carthroughputuser.png',bbox_inches='tight')

#AVG latency Drone-BS vs BS-Car
#Latency drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyD(result):
    #calculate latency drone-BS
    latencyD=0
    sumLatencyD=[]
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        #get info only from RX packets and sended by drone
        if (linesplit[4]=='1'):
            sumLatencyD.append((1000)*float(linesplit[10]))
    #do AVG
    latencyD=mean(sumLatencyD)
    
    return [latencyD]

    # Use the parsing function to create a Pandas dataframe
avg_latencyD = campaign.get_results_as_dataframe(get_latencyD, verbose=True )
display(avg_latencyD)

#Latency BS-Cars
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['average latency [ms]', 'CI_Diff'])
@sem.utils.output_labels(['average latency [ms]'])


def get_latencyC(result):    
    #calculate latency BS-UEs
    latencyC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ndelays=0
        #calculate the AVG delay for each UE
        for line in result['output']['DlPdcpStats.txt'].splitlines():
            linesplit=line.split()
            if (linesplit[4]==str(ue)):
                singleUE.append((1000)*float(linesplit[10]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    latencyC=mean(sumAVGUE)
    return [latencyC]

# Use the parsing function to create a Pandas dataframe
avg_latencyC= campaign.get_results_as_dataframe(get_latencyC,verbose=True )
display(avg_latencyC)

#Multiple latnecy into 1 plot
Latency_allinone=pd.DataFrame()
avg_latencyD['Type'] = 'Drone-BS'
avg_latencyC['Type'] = 'BS-Car'
Latency_allinone=Latency_allinone.append(avg_latencyD)
Latency_allinone=Latency_allinone.append(avg_latencyC)


sns.catplot(data=Latency_allinone,
            x='numUe',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.yscale("log")
plt.grid()
plt.title('average latency [ms] Drone-BS vs BS-Car')
plt.savefig(plotDir+'avg_latencyDrone-BSvsBS-Car.png',bbox_inches='tight')


Latency_allinone=Latency_allinone.sort_values(by=['fileSize'])
#File Size X axes: multiple latency into 1 plot
g=sns.catplot(data=Latency_allinone,
            x='fileSize',
            y='average latency [ms]',
            hue='Type',
            kind='point',
            height=5, aspect=4)
plt.xlabel("n vehicles")
g.set_xticklabels(rotation=80)
plt.yscale("log")
plt.grid()
plt.title('average latency [ms] Drone-BS vs BS-Car')
plt.savefig(plotDir+'avg_latencyFileSizeXDrone-BSvsBS-Car.png',bbox_inches='tight')

#Reliability Drone-BS vs BS-Car

#Reliability Drone-BS
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['AVG_TBler', 'CI_Diff'])
@sem.utils.output_labels(['AVG_TBler'])

def get_reliabilityD(result):
    #calculate reliability drone-BS
    tblerD=0
    sumTBlerD=[]
    for line in result['output']['RxPacketTrace.txt'].splitlines():
        linesplit=line.split('\t')
        #get info only from RX packets by BS and sended by drone
        if (linesplit[0]=='UL'and linesplit[8]=='1'):
            sumTBlerD.append(float(linesplit[15]))
    #do AVG 
    tblerD=mean(sumTBlerD)
    return [tblerD]

# Use the parsing function to create a Pandas dataframe
avg_error_rateD = campaign.get_results_as_dataframe(get_reliabilityD,verbose=True )
display(avg_error_rateD)

#Reliability BS-Car
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['AVG_TBler', 'CI_Diff'])
@sem.utils.output_labels(['AVG_TBler'])

def get_reliabilityC(result):
    #calculate reliability BS-UEs
    tblerC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        ntbler=0
        #calculate the AVG tbler for each UE
        for line in result['output']['RxPacketTrace.txt'].splitlines():
            linesplit=line.split('\t')
            if (linesplit[0]=='DL'and linesplit[8]==str(ue)):
                singleUE.append(float(linesplit[15]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
            #sum all the AVG of each single UE
            sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    tblerC=mean(sumAVGUE)
    return [tblerC]

    # Use the parsing function to create a Pandas dataframe
avg_error_rateC = campaign.get_results_as_dataframe(get_reliabilityC,verbose=True )
display(avg_error_rateC)

#Reliability Multiple into 1 plot
reliability_allinone=pd.DataFrame()
avg_error_rateD['Type'] = 'Drone-BS'
avg_error_rateC['Type'] = 'BS-Car'
reliability_allinone=reliability_allinone.append(avg_error_rateD)
reliability_allinone=reliability_allinone.append(avg_error_rateC)


sns.catplot(data=reliability_allinone,
            x='numUe',
            y='AVG_TBler',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability Drone-BS vs BS-Car')
plt.savefig(plotDir+'ReliabilityDrone-BSvsBS-Car.png',bbox_inches='tight')

#Pckt received from drone / pckt I want to send
#Pckt received from drone / pckt I want to send - Broadcast 30 fps
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_DB_broadcast(result):
    byteS   = result['params']['fileSize']*30; #byte for each second
    Npckts  = byteS/1500; #1500 max packet size in byte--> how many pckts to send in one second
    tot_pckt_to_send=Npckts*15 #15 is the simulation time
    #print(tot_pckt_to_send)
    #calculate pckt recevided from BS
    tot_packet_received=0
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]==str(1) and linesplit[17]==str(1530)):
            tot_packet_received=tot_packet_received+int(linesplit[8])
    #print(tot_packet_received)
    pckt_reliability_DB=(tot_packet_received/tot_pckt_to_send)*100
    if (pckt_reliability_DB>100):
        pckt_reliability_DB=100
    return [pckt_reliability_DB]

# Use the parsing function to create a Pandas dataframe
prob_pckt_DBS_broadcast = campaign.get_results_as_dataframe(pckt_reliability_DB_broadcast,verbose=True)
display(prob_pckt_DBS_broadcast)


#Pckt received from UEs / pckt send from BS --- Broadcast

@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['percentage of received packets'])


def pckt_reliability_BU(result):
    #calculate total pckt to send from BS
    pcktsintoFrame=result['params']['fileSize']/1500
    tot_packet_to_send=0
    for line in result['output']['UlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[4]==str(1) and linesplit[17]==str(1530)):
            tot_packet_to_send=tot_packet_to_send+int(linesplit[8])
    #Since is sended one anno for every time an entire frame arrive (frame is composed by multiple smaller pckts of 1500)
    tot_packet_to_send=tot_packet_to_send/pcktsintoFrame
    #calculate total pckt to send from BS
    tot_packet_to_receive=0
    maxue=0
    annosize=str((result['params']['numUe']*39.7)+30)
    for line in result['output']['DlPdcpStats.txt'].splitlines():
        linesplit=line.split()
        if (linesplit[3]!=str(1) and linesplit[17]==annosize.split(".")[0]):
            tot_packet_to_receive=tot_packet_to_receive+int(linesplit[8])   
            if(int(linesplit[4])>maxue):
                maxue=int(linesplit[4])
                
    tot_packet_to_send=tot_packet_to_send*(maxue-1) #so we can do a fair division since the first link is broadcast
    #print(tot_packet_received)
    pckt_reliability_BU=(tot_packet_to_receive/tot_packet_to_send)*100
    return [pckt_reliability_BU]


# Use the parsing function to create a Pandas dataframe
prob_pckt_BSUE = campaign.get_results_as_dataframe(pckt_reliability_BU, verbose=True )
display(prob_pckt_BSUE)

#reliability Multiple into 1 plot
prob_pckt_DBS_broadcast['Type'] = 'Drone-BS'
prob_pckt_BSUE['Type'] = 'BS-Car'
prob_pckt_DBS_broadcast=prob_pckt_DBS_broadcast.append(prob_pckt_BSUE)


sns.catplot(data=prob_pckt_DBS_broadcast,
            x='numUe',
            y='percentage of received packets',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('Reliability: pckt received/tot pckt to send Drone-BS vs BS-Car')
plt.savefig(plotDir+'Reliability_pckt_count.png',bbox_inches='tight')


#SNR all in one
#SNR
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['avg_SINR(dB)'])

def get_SNR(result):
    #calculate SINR drone-BS
    sinrD=0
    sumSinrD=[]
    for line in result['output']['RxPacketTrace.txt'].splitlines():
        linesplit=line.split('\t')
        #get info only from RX packets by BS and sended by drone
        if (linesplit[0]=='UL'and linesplit[8]=='1'):
            sumSinrD.append(float(linesplit[13]))
    #do AVG
    sinrD=mean(sumSinrD)
            
    #calculate SINR BS-UEs
    sinrC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        #calculate the AVG tbler for each UE
        for line in result['output']['RxPacketTrace.txt'].splitlines():
            linesplit=line.split('\t')
            if (linesplit[0]=='DL'and linesplit[8]==str(ue)):
                singleUE.append(float(linesplit[13]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    sinrC=mean(sumAVGUE)
    
    totalAVGSinr=(sinrD+sinrC)/2
    return [totalAVGSinr]

    # Use the parsing function to create a Pandas dataframe
avg_SNR = campaign.get_results_as_dataframe(get_SNR, verbose=True )
display(avg_SNR)

@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['avg_SINR(dB)'])
#SINR drone-BS
def get_SNRD(result):
    #calculate SINR drone-BS
    sinrD=0
    sumSinrD=[]
    for line in result['output']['RxPacketTrace.txt'].splitlines():
        linesplit=line.split('\t')
        #get info only from RX packets by BS and sended by drone
        if (linesplit[0]=='UL'and linesplit[8]=='1'):
            sumSinrD.append(float(linesplit[13]))
    #do AVG
    sinrD=mean(sumSinrD)
    return [sinrD]
# Use the parsing function to create a Pandas dataframe
avg_SNRD = campaign.get_results_as_dataframe(get_SNRD,verbose=True )
display(avg_SNRD)

#SINR BS-Car
@sem.utils.yields_multiple_results
#@sem.utils.output_labels(['avg_SINR(dB)', 'CI_Diff'])
@sem.utils.output_labels(['avg_SINR(dB)'])


def get_SNRC(result):
    #calculate SINR BS-UEs
    sinrC=0
    AVGSingleUe=0
    sumAVGUE=[]
    #for each UE
    for ue in range(2,result['params']['numUe']+2):
        singleUE=[]
        #calculate the AVG tbler for each UE
        for line in result['output']['RxPacketTrace.txt'].splitlines():
            linesplit=line.split('\t')
            if (linesplit[0]=='DL'and linesplit[8]==str(ue)):
                singleUE.append(float(linesplit[13]))
        if(not singleUE):
            AVGSingleUe=0
        else:
            AVGSingleUe=mean(singleUE)
        #sum all the AVG of each single UE
        sumAVGUE.append(AVGSingleUe)
    #do the AVG of the AVGs ==> sum(AVG for every single UE) / numUE
    sinrC=mean(sumAVGUE)
    
    return [sinrC]

# Use the parsing function to create a Pandas dataframe
avg_SNRC = campaign.get_results_as_dataframe(get_SNRC, verbose=True )
display(avg_SNRC)

#SINR Multiple into 1 plot
avg_SNR['Type'] = 'Total'
avg_SNRD['Type'] = 'Drone-BS'
avg_SNRC['Type'] = 'BS-Car'
avg_SNR=avg_SNR.append(avg_SNRD)
avg_SNR=avg_SNR.append(avg_SNRC)


sns.catplot(data=avg_SNR,
            x='numUe',
            y='avg_SINR(dB)',
            hue='Type',
            kind='point',
            height=5, aspect=2)
plt.xlabel("n vehicles")
plt.grid()
plt.title('avg_SINR (dB) Drone-BS vs BS-Car')
plt.savefig(plotDir+'avg_SINRDrone-BSvsBS-Car.png',bbox_inches='tight')
