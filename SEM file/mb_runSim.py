#AS PARAMETER THE NAME.TXT OF A SINGLE LABEL
import sys
import sem
import pprint
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import os
import shutil
import pandas as pd
#import cv2
sns.set_style("white")

#the number of labels could be less than the number of frames
#pass the label file name.txt= label
label=sys.argv[1]
parameters=[] #[0]= numUE, [1]=fileSize
df = pd.read_csv("all_labels_groundtruth/"+label, delimiter = "\t", header=None)
nUE=len(df.index)
parameters.append(nUE)
#search the file in the frames directory
x=label.split(".")
frame=x[0]+".jpg"
onlyframes = os.listdir("frames/")
if (frame in onlyframes):
    parameters.append(os.stat("frames/"+frame).st_size)

#Compile 
ns_path = '../../ns3-thesis'
script = 'DroneBSCar'
campaign_dir = "UDPAllFrames"
campaign = sem.CampaignManager.new(ns_path, script, campaign_dir, overwrite=False, check_repo=False)
print(campaign)

#RUN A SINGLE SIMULATION

params = {
    'numUe': parameters[0], #change with the variable listUE
    'simTime': 15, 
    'fileSize':parameters[1], #225000 byte = 225 kb #use the dictionary to get the 
    }

runs = 4

campaign.run_missing_simulations(params, runs=runs)